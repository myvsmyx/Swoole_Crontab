<?php
/**
 * 自己的第一个使用swoole写一个crontab
 * User: MikeMei
 * Date: 2016/8/24
 * Time: 17:46
 */
define('APP_DEBUG', true);
define('DS', DIRECTORY_SEPARATOR);
define('ROOTPATH', realpath(__DIR__).DS);

class Main{

	static private $_options = "hdrmp:s:l:c:";
	static private $_longopts = array("help", "daemon","reload", "monitor", "pid:", "log:", "config:","worker:","tasktype:","checktime:" );
	static private $_help = <<<EOF
帮助信息:
Usage: /path/to/php main.php [options] -- [args...]

-h [--help]        显示帮助信息
-p [--pid]         指定pid文件位置(默认pid文件保存在当前目录)
-s start           启动进程
-s stop            停止进程
-s restart         重启进程
-c [--config]      config文件的位置
-d [--daemon]      是否后台运行
-m [--monitor]     监控进程是否在运行,如果在运行则不管,未运行则启动进程
\n
EOF;

	/**
	 * 运行函数
	 */
	static public function run(){
		$opt = getopt( Main::$_options, Main::$_longopts );
		self::spl_autoload_register();
		self::params_h($opt);
		self::params_d($opt);
		self::params_p($opt);
		self::params_c($opt);
		self::params_m($opt);
		self::params_s($opt);
	}

	/**
	 * 载入使用方法
	 * @param $opt
	 */
	static public function params_h($opt){
		if( isset($opt['h']) || isset($opt['help']) ){
			exit(self::$_help);
		}
	}


	/**
	 * 设置运行模式
	 * @param $opt
	 */
	static public function params_d($opt){
		if( isset($opt['d']) || isset($opt['daemon']) ){
			Crontab::$daemon = true;
		}
	}


	/**
	 * 设置配置文件位置
	 * @param $opt
	 */
	static public function params_p($opt){
		if( isset($opt['p']) && $opt['p']){
			Crontab::$pidFile = $opt['p'].'/pid';
		}

		if( isset($opt['pid']) && $opt['pid']){
			Crontab::$pidFile = $opt['pid'].'/pid';
		}

		if( empty(Crontab::$pidFile) ){
			Crontab::$pidFile = ROOTPATH.'pid';
		}
	}

	/**
	 * 解析配置文件位置参数
	 * @param $opt
	 */
	static public function params_c($opt)
	{
		if (isset($opt["c"]) && $opt["c"]) {
			Crontab::$taskParams = $opt["c"];
		}
		if (isset($opt["config"]) && $opt["config"]) {
			Crontab::$taskParams = $opt["config"];
		}
		if (empty(Crontab::$taskParams)) {
			Crontab::$taskParams = ROOTPATH . "config/crontab.php";
		}
	}

	/**
	 * 启动方法
	 * @param $opt
	 */
	static public function params_s($opt){
		if( $opt['s'] && !in_array($opt['s'], array('start', 'restart', 'stop') ) ){
			Main::writelog('输入参数不正确啊');
		}
		switch($opt['s']){
			case 'start':
				Crontab::start();
			break;
			case 'stop':
				Crontab::stop();
			break;
			case 'restart':
				Crontab::restart();
			break;
			default:
				Main::writelog('输入参数不正确');
		}
	}


	/**
	 * 检测程序是否在运行中
	 * @param $opt
	 */
	static public function params_m($opt){
		if( isset($opt['m']) || isset($opt['monitor']) ){
			$pid = @file_get_contents(Crontab::$pidFile);
			if( $pid && swoole_process::kill($pid, 0) ){  //向子进程发送信号
				exit("运行中...\n");
			}else{
				exit("暂无结果...\n");
			}
		}
	}


	static public function writelog($msg){
		exit($msg."\n");
	}


	/**
	 * 注册类库载入路径
	 */
	static public function spl_autoload_register()
	{
		spl_autoload_register(function ($name) {
			$file_path = ROOTPATH .'Crontab'.DS. $name . ".class.php";
			if( !file_exists($file_path) ){
				$file_path = ROOTPATH .'Crontab'.DS.'LoadTask'.DS. $name . ".class.php";
				if( !file_exists($file_path) ){
					$file_path = ROOTPATH .'Crontab'.DS.'Plugin'.DS. $name . ".class.php";
				}
			}
			require($file_path);
		});
	}

}

Main::run();