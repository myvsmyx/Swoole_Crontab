<?php
return array(
	't1' =>
		array(
			'taskname' => 'writelog',  //任务名称
			'rule' => '*/1 * * * * *',//定时规则
			'phpcli' => 'php /data/wwwroot/mei/crontab1.php',//命令
		),
	't2' =>
		array(
			'taskname' => 'meiyi',  //任务名称
			'rule' => '*/1 * * * * *',
			'phpcli' => 'php /data/wwwroot/mei/crontab2.php',//命令
		),
);
