<?php
/**
 * Crontab 主类
 * User: MikeMei
 * Date: 2016/8/24
 * Time: 18:36
 */

class Crontab{

	static public $processName = 'mikemei';	//进程名称
	static public $daemon = false;		//运行模式
	static public $pidFile;				//pid文件位置
	static public $taskParams;			//task任务参数
	static public $tasksHandle;			//获取任务的句柄
	static private $pid;
	static public $unique_list = array();
	static public $task_list = array();


	/**
	 * 启动
	 */
	static public function start(){
		if( file_exists(self::$pidFile) ){
			exit("pid文件已经存在\n");
		}
		self::setDaemon();
		self::setProcessName();
		self::run();
		echo "启动成功\n";
	}

	/**
	 * 停止
	 */
	static public function stop(){
		$pid = @file_get_contents(self::$pidFile);
		if( $pid ){
			if( swoole_process::kill($pid, 0 ) ){
				swoole_process::kill($pid, SIGTERM);
				echo "进程{$pid}已结束\n";
			}else{
				@unlink(self::$pidFile);
				echo "进程{$pid}不存在, 已经删除pid文件\n";
			}
		}else{
			echo "需要停止的进程未启动\n";
		}
	}

	/**
	 * 重启
	 */
	static public function restart(){
		self::stop();
		sleep(1);
		self::start();
	}

	/**
	 *	设置当前进程为守护进程
	 */
	static public function setDaemon(){
		if( self::$daemon ){
			swoole_process::daemon();
		}
	}

	/**
	 * 设置进程名称
	 */
	static public function setProcessName(){
		if (!function_exists("swoole_set_process_name")) {
			self::exit2p("Please install swoole extension.http://www.swoole.com/");
		}
		swoole_set_process_name(self::$processName);
	}

	/**
	 * 正式运行
	 */
	static public function run(){
		try{
			self::$tasksHandle = new LoadTask(self::$taskParams);//载入配置文件
		}catch (Exception $e){
			exit($e->getMessage());
		}
		self::register_signal();
		self::load_config();
		self::register_timer();
		self::get_pid();
		self::write_pid();
	}


	/**
	 * 注册信号
	 */
	static private function register_signal(){
		swoole_process::signal(SIGTERM, function ($signo) { //设置异步信号监听
			self::exit2p("收到退出信号,退出主进程");
		});

		swoole_process::signal(SIGCHLD, function ($signo) {//设置异步信号监听  SIGCHLD 子进程结束时, 父进程会收到这个信号
			while ($ret = swoole_process::wait(false)) { //回收结束运行的子进程
				$pid = $ret['pid'];
				if (isset(self::$task_list[$pid])) {
					$task = self::$task_list[$pid];
					$id = $task["id"];
					$task["process"]->close();//关闭进程
					unset(self::$task_list[$pid]);
				}
			};
		});
	}

	/**
	 * 根据配置载入需要执行的任务
	 */
	static public function load_config(){
		$time = time();
		$config = self::$tasksHandle->getTasks();
		foreach ($config as $id => $task) {
			$ret = ParseCrontab::parse($task["rule"], $time);
			if ($ret === false) {
				echo "任务时间解析错误\n";
			} elseif (!empty($ret)) {
				TickTable::set_task($ret, array_merge($task, array("id" => $id)));
			}
		}
	}

	/**
	 *  注册定时任务
	 */
	static protected function register_timer(){
		swoole_timer_tick(60000, function () {//设置一个间隔时钟定时器
			self::load_config();
		});
		swoole_timer_tick(1000, function ($interval) {
			self::runTask($interval);
		});
	}

	/**
	 * 运行任务
	 * @param $interval
	 * @return bool
	 */
	static public function runTask($interval)
	{
		$tasks = TickTable::get_task();
		if (empty($tasks)) return false;
		foreach ($tasks as  $task) {
			$process = new ProcessTask();
			$process->create_process($task["id"], $task);
		}
		return true;
	}

	/**
	 * 过去当前进程的pid
	 */
	static private function get_pid(){
		if (!function_exists("posix_getpid")) {
			self::exit2p("Please install posix extension.");
		}
		self::$pid = posix_getpid();
	}


	/**
	 * 写入当前进程的pid到pid文件
	 */
	static private function write_pid(){
		file_put_contents(self::$pidFile, self::$pid);
	}

	/**
	 * 退出进程口
	 * @param $msg
	 */
	static private function exit2p($msg){
		@unlink(self::$pidFile);
		exit($msg);
	}

}