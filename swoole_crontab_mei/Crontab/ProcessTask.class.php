<?php
/**
 * 进程相关
 */

class ProcessTask
{
	public $task;

	/**
	 * 创建一个子进程
	 * @param $task
	 */
	public function create_process($id, $task)
	{
		$this->task = $task;
		$process = new swoole_process(array($this, "run")); //创建子进程
		$pid = $process->start();//执行fork系统调用，启动进程
		if ( !$pid ) {
			//创建子进程失败
		}
		//记录当前任务
		Crontab::$task_list[$pid] = array(
			"start" => microtime(true),
			"id" => $id,
			"task" => $task,
			"type" => "crontab",
			"process" =>$process,
		);
	}

	/**
	 * 子进程执行的入口
	 * @param $worker
	 */
	public function run(swoole_process $worker)
	{
		//$class = $this->task["execute"];
		$worker->name('hy_crontab_'.$this->task["id"]);
		$c = new Cmd;
		$c->run($this->task["phpcli"]);
		$this->_exit($worker);
	}

	private function _exit(swoole_process $worker)
	{
		$worker->exit(1);//退出子进程
	}
}

