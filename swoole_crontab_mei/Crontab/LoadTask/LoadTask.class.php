<?php
/**
 * 加载任务类
 * User: MikeMei
 * Date: 2016/8/26
 * Time: 16:36
 */

class LoadTask
{
	private $_filePath; //配置文件路径
	private $_oriTasks;

	public function __construct($file)
	{
		if( !file_exists($file) ){
			throw new Exception("crontab配置文件不存在\n");
		}
		$this->_filePath = $file;
	}

	/**
	 * 获取需要执行的任务
	 * @return array
	 */
	public function getTasks()
	{
		$this->_oriTasks = include($this->_filePath);
		$tasks = array();
		if (is_array($this->_oriTasks)) {
			foreach ($this->_oriTasks as $key => $val) {
				$tasks[$key] = array(
					"taskname" => $val["taskname"],
					"rule" => $val["rule"],
					"phpcli" => $val["phpcli"]
				);
			}
		}
		return $tasks;
	}

}