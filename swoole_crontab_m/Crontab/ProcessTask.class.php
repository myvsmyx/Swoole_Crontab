<?php
/**
 * 进程相关
 */

namespace Crontab;
use Crontab\Crontab;
use Swoole\Process;


class ProcessTask
{
	public $task;

	/**
	 * 创建一个子进程
	 * @param $task
	 */
	public function create_process($id, $task)
	{
		$this->task = $task;
		$process = new swoole_process(array($this, "run")); //创建子进程
		if (!($pid = $process->start())) {

		}
		//记录当前任务
		Crontab::$task_list[$pid] = array(
			"start" => microtime(true),
			"id" => $id,
			"task" => $task,
			"type" => "crontab",
			"process" =>$process,
		);
	}

	/**
	 * 子进程执行的入口
	 * @param $worker
	 */
	public function run(swoole_process $worker)
	{
		$class = $this->task["execute"];
		$worker->name("hy_crontab_" . $class . "_" . $this->task["id"]);
		$c = new $class;
		$c->worker = $worker;
		$c->run($this->task["args"]);
		$this->_exit($worker);
	}

	private function _exit(swoole_process $worker)
	{
		$worker->exit(1);//退出子进程
	}
}

