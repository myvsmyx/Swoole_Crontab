<?php
/**
 * Crontab 主类
 * User: MikeMei
 * Date: 2016/8/24
 * Time: 18:36
 */

namespace Crontab;

use Swoole\Process;
use Crontab\TickTable;
use Crontab\ParseCrontab;
use Crontab\ProcessTask;
use Crontab\LoadTask\LoadTask;
use Crontab\Plugin\Cmd;

class Crontab{

	static public $processName = '';	//进程名称
	static public $daemon = false;		//运行模式
	static public $pidFile;				//pid文件位置
	static public $taskParams;			//task任务参数
	static public $tasksHandle;			//获取任务的句柄
	static private $pid;


	static private $swooleProcess;

	/**
	 * 启动
	 */
	static public function start(){
		if( !self::$swooleProcess ){
			self::$swooleProcess = new process(function (){});
		}
		if( file_exists(static::$pidFile) ){
			exit("pid文件已经存在\n");
		}

//		self::setDaemon();
//		self::setProcessName();
		self::run();
	}

	/**
	 *	设置当前进程为守护进程
	 */
	static public function setDaemon(){
		if( self::$daemon ){
			swoole_process::daemon();
		}
	}

	/**
	 * 设置进程名称
	 */
	static public function setProcessName(){
		if (!function_exists("swoole_set_process_name")) {
			self::exit2p("Please install swoole extension.http://www.swoole.com/");
		}
		swoole_set_process_name(self::$processName);
	}

	/**
	 * 正式运行
	 */
	static public function run(){
		try{
			self::$tasksHandle = new LoadTask(self::$taskParams);//载入配置文件
		}catch (Exception $e){
			exit($e->getMessage());
		}
		self::register_signal();
		self::load_config();
		self::register_timer();
		self::get_pid();
		self::write_pid();
	}


	/**
	 * 注册信号
	 */
	static private function register_signal(){
		self::$swooleProcess->signal(SIGTERM, function ($signo) {
			self::exit2p("收到退出信号,退出主进程");
		});


		self::$swooleProcess->signal(SIGCHLD, function ($signo) {//设置异步信号监听  SIGCHLD 子进程结束时, 父进程会收到这个信号
			while ($ret =self::$swooleProcess->wait(false)) { //回收结束运行的子进程
				$pid = $ret['pid'];
				if (isset(self::$task_list[$pid])) {
					$task = self::$task_list[$pid];
					if ($task["type"] == "crontab") {
						$end = microtime(true);
						$start = $task["start"];
						$id = $task["id"];
						$task["process"]->close();//关闭进程
						unset(self::$task_list[$pid]);
						if (isset(self::$unique_list[$id]) && self::$unique_list[$id] > 0) {
							self::$unique_list[$id]--;
						}
					}
				}
			};
		});
	}

	/**
	 * 退出进程口
	 * @param $msg
	 */
	static private function exit2p($msg){
		@unlink(self::$pid_file);
		exit();
	}

	/**
	 * 根据配置载入需要执行的任务
	 */
	static public function load_config(){
		$time = time();
		$config = self::$tasksHandle->getTasks(self::$taskParams);
		foreach ($config as $id => $task) {
			$ret = ParseCrontab::parse($task["rule"], $time);
			if ($ret === false) {
				//Main::log_write(ParseCrontab::$error);
			} elseif (!empty($ret)) {
				TickTable::set_task($ret, array_merge($task, array("id" => $id)));
			}
		}
	}

	/**
	 *  注册定时任务
	 */
	static protected function register_timer(){
		swoole_timer_tick(60000, function () {//设置一个间隔时钟定时器
			self::load_config();
		});
		swoole_timer_tick(1000, function ($interval) {
			self::runTask($interval);
		});
	}

	/**
	 * 运行任务
	 * @param $interval
	 * @return bool
	 */
	static public function runTask($interval)
	{
		$tasks = TickTable::get_task();
		if (empty($tasks)) return false;
		foreach ($tasks as  $task) {
			if (isset($task["unique"]) && $task["unique"]) {
				if (isset(self::$unique_list[$task["id"]]) && (self::$unique_list[$task["id"]] >= $task["unique"])) {
					continue;
				}
				self::$unique_list[$task["id"]] = isset(self::$unique_list[$task["id"]]) ? (self::$unique_list[$task["id"]] + 1) : 0;
			}
			$process = new Process();
			$process->create_process($task["id"], $task);
		}
		return true;
	}


	/**
	 * 过去当前进程的pid
	 */
	static private function get_pid(){
		if (!function_exists("posix_getpid")) {
			self::exit2p("Please install posix extension.");
		}
		self::$pid = posix_getpid();
	}

	/**
	 * 写入当前进程的pid到pid文件
	 */
	static private function write_pid(){
		file_put_contents(self::$pidFile, self::$pid);
	}

}