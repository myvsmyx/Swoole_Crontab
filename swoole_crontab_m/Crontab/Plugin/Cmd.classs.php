<?php

/**
 * CMD运行相关函数
 */
namespace Crontab\Plugin;

class  Cmd
{

	public function run($task)
	{
		$cmd = $task["cmd"];
		$status = 0;
		exec($cmd, $output, $status);
		exit($status);
	}
}