<?php
/**
 * 自己的第一个使用swoole写一个crontab
 * User: MikeMei
 * Date: 2016/8/24
 * Time: 17:46
 */
define('APP_DEBUG', true);
define('DS', DIRECTORY_SEPARATOR);
define('ROOTPATH', realpath(__DIR__).DS);

use Crontab\Crontab;

class Main{

	static private $_options = "hdrmp:s:l:c:";
	static private $_longopts = array("help", "daemon","reload", "monitor", "pid:", "log:", "config:","worker:","tasktype:","checktime:" );
	static private $_help = <<<EOF
帮助信息:
Usage: /path/to/php main.php [options] -- [args...]

-h [--help]        显示帮助信息
-p [--pid]         指定pid文件位置(默认pid文件保存在当前目录)
-s start           启动进程
-s stop            停止进程
-s restart         重启进程
-l [--log]         log文件夹的位置
-c [--config]      config文件的位置
-d [--daemon]      是否后台运行
-r [--reload]      重新载入配置文件
-m [--monitor]     监控进程是否在运行,如果在运行则不管,未运行则启动进程
--worker           开启worker
--tasktype         task任务获取类型,[file|mysql] 默认是file
--checktime        默认精确对时(如果精确对时,程序则会延时到分钟开始0秒启动) 值为false则不精确对时
\n
EOF;

	/**
	 * 运行函数
	 */
	static public function run(){
		$opt = getopt( Main::$_options, Main::$_longopts );
		self::spl_autoload_register();
		self::params_h($opt);
//		self::params_d($opt);
//		self::params_p($opt);
//		self::params_l($opt);
		self::params_c($opt);
//		self::params_r($opt);
//		self::params_worker($opt);
//		self::params_tasktype($opt);
//		self::params_checktime($opt);
//		$opt = self::params_m($opt);
		self::params_s($opt);

	}

	static public function params_h($opt){
		if( isset($opt['h']) || isset($opt['help']) ){
			exit(self::$_help);
		}
	}

	static public function params_d($opt){
		if( isset($opt['d']) || isset($opt['daemon']) ){
			//
		}
	}

	/**
	 * 解析配置文件位置参数
	 * @param $opt
	 */
	static public function params_c($opt)
	{
		if (isset($opt["c"]) && $opt["c"]) {
			Crontab::$taskParams = $opt["c"];
		}
		if (isset($opt["config"]) && $opt["config"]) {
			Crontab::$taskParams = $opt["config"];
		}
		if (empty(Crontab::$taskParams)) {
			Crontab::$taskParams = ROOTPATH . "config/crontab.php";
		}
	}


	static public function params_s($opt){
		if( $opt['s'] && !in_array($opt['s'], array('start', 'restart', 'stop') ) ){
			Main::writelog('输入参数不正确啊');
		}
		switch($opt['s']){
			case 'start':
				Crontab::start();
			break;
			case 'stop':
				//
			break;
			case 'restart':
				//
			break;
			default:
				Main::writelog('输入参数不正确啊');
		}
	}

	static public function writelog($msg){
		exit($msg."\n");
	}


	/**
	 * 注册类库载入路径
	 */
	static public function spl_autoload_register()
	{
		spl_autoload_register(function ($name) {
			$file_path = ROOTPATH . $name . ".class.php";
			$file_path = str_replace("\\", '/', $file_path);

			if( file_exists($file_path) ){
				//exit($file_path."不存在\n");
				require($file_path);
			}
		});
	}

}

Main::run();