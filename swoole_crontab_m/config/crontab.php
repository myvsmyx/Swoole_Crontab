<?php
return array(
	't1' =>
		array(
			'taskname' => 'writelog',  //任务名称
			'rule' => '*/1 * * * * *',//定时规则
			"unique" => 1, //排他数量，如果已经有这么多任务在执行，即使到了下一次执行时间，也不执行
			'execute'  => 'Cmd',//命令处理类
			'args' =>
				array(
					'cmd'    => 'php /data/wwwroot/mei/crontab1.php',//命令
					'ext' => '',//附加属性
				),
		),
	't2' =>
		array(
			'taskname' => 'meiyi',  //任务名称
			'rule' => '*/5 * * * * *',
			"unique" => 1, //排他数量，如果已经有这么多任务在执行，即使到了下一次执行时间，也不执行
			"execute" =>"Cmd",
			'args' =>
				array(
					'cmd'    => 'php /data/wwwroot/mei/crontab2.php',//命令
					'ext' => '',//附加属性
				),
		),
);
